from wtforms.ext.appengine.ndb import model_form
from wtforms.validators import ValidationError
from flask.ext.wtf import Form
from model import Note


def DateUserUniqueValidator(form, date):
    user = form.user.data
    if Note.get_by_user_date(user, date):
        ValidationError("you already have note for this day")

NoteForm = model_form(Note, base_class=Form,  only=('text', 'date', 'public', 'user'), field_args={
    'date': {
        'validators': [DateUserUniqueValidator]
    },
    'user': {

    }
})

